import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

// Lớp biểu diễn 1 bàn chơi
public class Game {

	// Các đối tượng người chơi
	private Player playerX;
	private Player playerO;

	// Hàm khởi tạo 1 bàn chơi, cần có 2 người chơi
	public Game(Player playerX, Player playerO) {

		this.playerX = playerX;
		this.playerO = playerO;

		// Đặt đối thủ cho người chơi này là người chơi kia và ngược lại
		playerX.setOpponent(playerO);
		playerO.setOpponent(playerX);

	}

	// Bắt đầu ván
	public void start() {
		playerX.output.println("START_GAME_WITH O " + playerO.getPlayerName());
		playerO.output.println("START_GAME_WITH X " + playerX.getPlayerName());

		// Nếu quân cờ của người chơi là X thì thông báo cho client đc chơi
		// trước.
		playerX.output.println("MESSAGE Your move");
		playerX.setFree(false);
		playerO.setFree(false);
		System.out.println("START GAME: PlayerX = " + playerX.getPlayerName()
				+ ", PlayerO= " + playerO.getPlayerName());
	}

	// Lớp biểu diễn 1 người chơi
	// Mỗi người chơi là 1 Thread để nó chạy trên 1 luồng khác
	static class Player extends Thread {

		// Đối thủ
		private Player opponent;

		// Socket kết nối với client
		private Socket socket;
		// Quân cờ
		private char mark;

		private String name;

		// Các đối tượng phục vụ cho việc đọc lệnh từ client gửi lên
		// và truyền lệnh từ server xuống client
		private BufferedReader input;
		private PrintWriter output;

		// Trạng thái người chơi đang chơi hay rảnh
		// true nếu rảnh
		private boolean isFree;

		// Hàm khởi tạo 1 người chơi
		// Cần truyền vào 1 đối tượng socket kết nối từ client
		public Player(Socket socket) {
			this.socket = socket;

			try {
				input = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				output = new PrintWriter(socket.getOutputStream(), true);
			} catch (IOException e) {

			}
		}

		// Đặt quân cờ cho người chơi
		public void setMark(char mark) {
			this.mark = mark;

			// Thông báo cho client biết quân cờ của mình
			output.println("WELCOME " + mark);
		}

		// Đặt đối thủ cho người chơi
		public void setOpponent(Player opponent) {
			this.opponent = opponent;
		}

		// Lấy đổi thủ của người chơi
		public Player getOpponent() {
			return opponent;
		}

		public boolean isFree() {
			return this.isFree;
		}

		public void setFree(boolean free) {
			this.isFree = free;
		}

		// Hàm này chạy liên tục khi Game được start
		@Override
		public void run() {
			System.out.println("Player " + getPlayerName() + " started");
			try {
				// Khi game được start, thông báo cho client là đã kết nối được
				// với server
				output.println("CONNECTED");
				output.flush();

				// Trạng thái khi mới kết nối của người chơi là free
				this.isFree = true;

				// Gửi danh sách người chơi hiện tại về client
				/* getUserList(getPlayerName()); */

				// Cập nhật danh sách người chơi tại tất cả các client
				updatePlayerListAtAllClient();

				// Repeatedly get commands from the client and process them.
				while (true) {
					// Chờ đọc một lệnh từ client gửi lên
					String command = input.readLine();
					System.out.println("CLIENT " + name + " COMMAND: "
							+ command);

					/*
					 * if (command != null) {
					 * System.out.println("CLIENT COMMAND: " + command); }
					 */

					// Nếu lệnh bắt đầu bằng MOVE -> client thực hiện 1 lượt
					// đánh
					// Ví dụ: MOVE 25
					if (command.startsWith("MOVE")) {
						// Lấy vị trí client đánh ở trong lệnh
						int location = Integer.parseInt(command.substring(5));
						// Thông báo cho đối thủ biết người chơi vừa đánh
						if (getOpponent() != null) {
							getOpponent().opponentMove(location);
						}
					} // Nếu lệnh là QUIT, người chơi thoát game
					else if (command.startsWith("QUIT")) {
						if (getOpponent() != null)
							// Thông báo cho đối thủ biết đối phương đã thoát
							getOpponent().opponentQuit();

						// Xóa người chơi ra khỏi PlayerPool
						PlayerPool.getInstance().removePlayer(this);
						socket.close();
						return;
					} // Nếu lệnh bắt đầu bằng USERNAME, thiết đặt username
					else if (command.startsWith("USERNAME")) {
						setPlayerName(command.substring(9));
					} // Lấy danh sách bạn bè
					else if (command.startsWith("GET_PLAYER_LIST#")) {
						updateUserList(command.substring(17));
					} // Gửi lời mời chơi đến người khác
					else if (command.startsWith("SEND_INVITATION_TO")) {
						sendInvitation(command.substring(19));
					} // Người chơi chấp nhận lời mời
					else if (command.startsWith("ACCEPT_INVITATION_FROM")) {
						startNewGame(command.substring(23));
					} // Người chơi từ chối lời mời
					else if (command.startsWith("REJECT_INVITATION_FROM")) {
						Player otherSide = PlayerPool.getInstance()
								.getPlayerByUserName(command.substring(23));
						otherSide.output.println("INVITATION_REJECTED_BY "
								+ this.getPlayerName());
						otherSide.output.flush();
					} // Khi một người không muốn tiếp tục chơi
					else if (command.startsWith("END_GAME")) {
						clearInformationPreviousGame();
					}
				}
			} catch (Exception e) {
				// Khi xảy ra lỗi thì xóa Player khỏi PlayerPool
				System.out.println("Player " + getPlayerName() + " died");
				e.printStackTrace();
				
				PlayerPool.getInstance().removePlayer(this);
				if (getOpponent() != null) {
					getOpponent().setOpponent(null);
				}
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}

		private void clearInformationPreviousGame() {
			this.mark = '-';
			this.setFree(true);
			this.getOpponent().setFree(true);

			// Gửi thông báo đến đối thủ khi không muốn chơi tiếp
			this.getOpponent().output.println("END_GAME_FROM " + this.name);
			this.getOpponent().output.flush();

			this.getOpponent().setOpponent(null);
			this.setOpponent(null);

			updatePlayerListAtAllClient();
		}

		// Update user list tại tất cả các client
		private void updatePlayerListAtAllClient() {
			List<Player> players = PlayerPool.getInstance().getAllPlayers();
			for (Player player : players) {
				player.updateUserList(player.getPlayerName());
			}
		}

		// Tạo một game giữa 2 người chơi
		private void startNewGame(String opponentName) {
			Player opponent = PlayerPool.getInstance().getPlayerByUserName(
					opponentName);

			Game game = new Game(this, opponent);
			game.start();

			// update lại trạng thái các người chơi tại tất cả client
			updatePlayerListAtAllClient();
		}

		// Gửi lời mời đến đối thủ
		private void sendInvitation(String username) {
			Game.Player player = PlayerPool.getInstance().getPlayerByUserName(
					username);
			player.output.println("INVITATION_FROM " + getPlayerName());
			player.output.flush();
		}

		private void updateUserList(String currentUserName) {
			List<String> players = PlayerPool.getInstance().getPlayerList(
					currentUserName);
			String users = "PLAYER_LIST ";
			for (String item : players) {
				users += item + ",";
			}

			System.out.println("CLIENT " + name + " " + users);
			output.println(users);
		}

		// Thông báo tên đối thủ
		private void setOpponentName(String name) {
			// Thông báo cho client biết tên đối thủ
			output.println("OPPONENT_NAME " + name);
		}

		// Thông báo đổi thủ đi 1 nước
		public void opponentMove(int location) {
			// Thông báo xuống client
			output.println("OPPONENT_MOVE " + location);
		}

		// Thông báo đối thủ thoát
		public void opponentQuit() {
			// Thông báo xuống client
			output.println("OPPONENT_QUIT");
			setOpponent(null);
		}

		// Thông báo đợi người chơi
		public void sendWaitingMessage() {
			output.println("WAITING");
		}

		public String getPlayerName() {
			return name;
		}

		public void setPlayerName(String name) {
			this.name = name;
		}
	}
}
