import java.util.ArrayList;
import java.util.List;

// Lớp chứa tất cả các người chơi kết nối vào game
// Tạo bàn chơi
public class PlayerPool {

	private static PlayerPool instance;

	// Mảng các người chơi tham gia vào game
	private List<Game.Player> players = new ArrayList<>();

	// Chúng ta lấy 1 đối tượng của lớp PlayerPool thông qua hàm này,
	// đảm bảo chỉ có 1 đối tương PlayerPool được tạo ra
	public static synchronized PlayerPool getInstance() {
		if (instance == null) {
			instance = new PlayerPool();
		}

		return instance;
	}

	// Thêm một người chơi mới, kiểm tra người chơi đang rảnh vào tạo bàn chơi
	public synchronized void addPlayer(Game.Player newPlayer) {
		// Thêm người chơi mới vào mảng
		players.add(newPlayer);
		newPlayer.start();
	}

	// Xóa 1 người chơi ra khỏi PlayerPool
	public void removePlayer(Game.Player player) {
		players.remove(player);
	}

	// Lấy danh sách các người chơi ngoại trừ người chơi hiện tại
	// kèm trạng thái isFree
	public List<String> getPlayerList(String currentUserName) {
		List<String> names = new ArrayList<>();
		for (Game.Player player : players) {
			if (!player.getPlayerName().equals(currentUserName)){
				names.add(player.getPlayerName() + "-" + player.isFree());
			}				
		}
		return names;
	}

	public List<Game.Player> getAllPlayers() {
		return this.players;
	}

	public Game.Player getPlayerByUserName(String username) {
		for (Game.Player player : players) {
			if (player.getPlayerName().equals(username)) {
				return player;
			}
		}
		return null;
	}

	/**
	 * Kiểm tra xem username đăng ký có tồn tại hay chưa. Trả về true nếu đã tồn
	 * tại, ngược lại là false
	 * 
	 * @param username
	 * @return
	 */
	public boolean checkUserIfExisted(String username) {
		for (Game.Player player : players) {
			if (player.getPlayerName().equals(username)) {
				return true;
			}
		}
		return false;
	}
}
