import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) {

		try {

			// Lắng nghe ở port 12345
			@SuppressWarnings("resource")
			ServerSocket listener = new ServerSocket(12345);
			System.out.println("Tic Tac Toe Server is Running");

			// Lặp vô hạn
			while (true) {
				// Chờ 1 người chơi kết nối
				Socket socket = listener.accept();

				//Kiểm tra xem username đã tồn tại hay chưa
				String username = null;
				while(true){
					username = checkUserNameIfExisted(socket);
					System.out.println("Username is existed: " + (username == null));
					if (username != null) break;
				}
				
				// Nếu người chơi chưa tồn tại thì thêm vào danh sách
				if (username != null) {
					Game.Player newPlayer = new Game.Player(socket);
					newPlayer.setPlayerName(username);
					// Thêm người chơi vào PlayerPool
					PlayerPool.getInstance().addPlayer(newPlayer);
				}

			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Kiểm tra xem username có tồn tại hay chưa 
	 * Trả về username nếu nó chưa tồn tại
	 * Ngược lại trả về null
	 * 
	 * @param socket
	 * @return
	 */
	public static String checkUserNameIfExisted(Socket socket) {
		BufferedReader input = null;
		PrintWriter output = null;
		try {
			input = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream());
			String command;
			command = input.readLine();
			System.out.println("CLIENT COMMAND: " + command);
			if (command.startsWith("USERNAME")) {
				String username = command.substring(9);
				if (PlayerPool.getInstance().checkUserIfExisted(username)) {
					output.println("USERNAME_EXISTED " + username);
					output.flush();
					return null;
				} else {
					return username;
				}
			} else {
				output.println("CLIENT_ERROR");
				output.flush();
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
			if (output != null) {
				output.println("SERVER_ERROR");
				output.flush();
			}
			try {
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return null;
		}

	}
}
